const { ipcMain } = require('electron')
const request = require('request')

ipcMain.on('login', (event, arg) => {

    const headers = {
        'Content-Type': 'application/json'
    }
    const options = {
        url: 'http://api.theteller.net/corporate/login',
        method: 'POST',
        headers: headers,
        form: arg
    }
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            event.returnValue = body
        }
    })

}) 
