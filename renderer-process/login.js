const settings = require('electron-settings')
const encrypter = require('object-encrypter');
const net = require('axios')
const engine = encrypter('394rwe78fudhwqpwriufdhr8ehyqr9pe8fud', {
    ttl: true
});

const {
    ipcRenderer
} = require('electron')

const loginButton = document.getElementById('loginButton')

loginButton.addEventListener('click', (event) => {
    login();
})

$(() => {
    $('#noLogin').show();
    $('#yesLogin').hide();

})

function login() {
    $('#loader1').show();

    var id = $('#merchant-id').val();
    var pass = $('#merchant-pass').val();

    const credentials = {
        "mid": id,
        "pass": pass
    }
    const payLoad = engine.encrypt(credentials, 60000);
    const data = {
        "pl": payLoad
    }

    net.post('http://localhost:8000/login', data)
        .then(function (response) {
            console.log(response);
            $('#loader1').hide();
            Snackbar.show({ text: 'success', showAction: false, pos: 'bottom-center' });
        })
        .catch(function (error) {
            console.log(error);
            $('#loader1').hide();
            Snackbar.show({ text: 'error', showAction: false, pos: 'bottom-center' });
        });
}


